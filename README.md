In the medical cannabis space, A Green Relief and its team can help you find alternative options for your pain relief. Dr. Rodriguez is their on site doctor who can diagnose and prescribe medical marijuana for pain management and relief. They serve the greater Kissimmee and Orlando with 2 locations.

Address: 201 Hilda St, #21, Kissimmee, FL 34741, USA

Phone: 407-572-8643
